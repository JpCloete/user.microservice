﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Pogga.ClassTypeContracts.Models;
using ResponseSerialization;
using Users.Microservice.Models;

namespace Users.Microservice.Controllers
{
    public class ResetUserNameController : Controller
    {
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly UserDbContext _db;
        private readonly ISerializationService _serialization;  
        public ResetUserNameController(UserManager<User> userManager, SignInManager<User> signInManager, UserDbContext db, ISerializationService serialization)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _db = db;
            _serialization = serialization;
        }

        public class UserNameChangeUserWithPassword : UserWithPassword
        {
            public virtual string NewUserName { get; set; }
        }

                            
        [HttpPost]
        [Route("/user/reset-username")]
        public async Task<string> ResetUserNameAsync([FromBody] UserNameChangeUserWithPassword userNameChangeUserWithPassword)
        {
            if (string.IsNullOrEmpty(userNameChangeUserWithPassword.UserName) ||
                string.IsNullOrEmpty(userNameChangeUserWithPassword.Password) ||
                string.IsNullOrEmpty(userNameChangeUserWithPassword.NewUserName))       
            {
                return _serialization.SerializeMessage(400, "Invalid Request");
            }

            User user = await _db.Users.Where(x => x.UserName == userNameChangeUserWithPassword.UserName).FirstOrDefaultAsync();
            if(user == null)
            {
                return _serialization.SerializeMessage(401, "Invalid Credentials");
            }

            if(user.UserNameChangesRemaining != 1)
                return _serialization.SerializeMessage(500, "Invalid Request, you can only change your Username once every 24 hours.");

            var signInResult = await _signInManager.CheckPasswordSignInAsync(user, userNameChangeUserWithPassword.Password, false);

            if (signInResult.Succeeded)
            {  
                var result = await _userManager.SetUserNameAsync(user, userNameChangeUserWithPassword.NewUserName);

                if (result.Succeeded)
                {
                    User updatedUser = user;

                    updatedUser.UserNameChangesRemaining = 0;
                    updatedUser.UserNameChangeCooldown = DateTime.UtcNow.AddDays(1);
                    _db.Users.Attach(updatedUser);
                    _db.Entry(updatedUser).Property(x => x.UserNameChangesRemaining).IsModified = true;
                    _db.Entry(updatedUser).Property(x => x.UserNameChangeCooldown).IsModified = true;
                    await _db.SaveChangesAsync();

                    return "Username Change Successful";
                }
                else
                {
                    return _serialization.SerializeMessage(400, result.Errors.ToString());
                }
            } 
            else
            {
                return _serialization.SerializeMessage(401, "Invalid credentials");
            }
        }
    }
}
