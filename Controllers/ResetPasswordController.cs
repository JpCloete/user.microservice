﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using NETCore.MailKit.Core;
using Pogga.ClassTypeContracts.Models;
using ResponseSerialization;
using Users.Microservice.Interfaces;
using Users.Microservice.Models;

namespace Users.Microservice.Controllers
{

    public class ResetPasswordController : Controller
    {
        private readonly UserManager<User> _userManager;
        private readonly IEmailSenderService _emailService;
        private readonly UserDbContext _db;
        private readonly CheckPasswordService _passwordService;
        private readonly ISerializationService _serialization;
        public ResetPasswordController(UserManager<User> userManager, EmailSenderService emailService, UserDbContext db, CheckPasswordService passwordService, ISerializationService serialization)
        {
            _userManager = userManager;
            _emailService = emailService;
            _db = db;
            _passwordService = passwordService;
            _serialization = serialization;
        }

        [HttpPost]
        [Route("/user/reset-password-request")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<string> ChangePasswordRequestAsync([FromBody] UserWithPassword userWithPassword)
        {
            (User?, string) userTuple = await _passwordService.CheckUserPasswordAsync(userWithPassword);
            if(userTuple.Item1 == null)
            {
                return _serialization.SerializeMessage(401, "Invalid Credentials");
            }

            User user = userTuple.Item1;
            var token = await _userManager.GeneratePasswordResetTokenAsync(user);
            string tokenEcoded = HttpUtility.UrlEncode(token);
            IConfiguration config = Factory.InstantiateNewConfiguration().GetConfiguration();
            string gatewayUrl = config.GetSection("Config").GetSection("GatewayApiUrl").Value;
            Uri link = new Uri("https://" + gatewayUrl + "/user/reset-password-form" + "?userId=" + user.Id.ToString() + "&token=" + tokenEcoded);

            await _emailService.SendEmail("test@test.com", "Reset Password", $"<a href=\"{link}\">Click here to reset your account's password.<a>", true);

            return "Click the Reset Password link sent to your Account's Email.";
        }

        //Verifies if password reset token is valid, redirects to form and gives access based if token is valid.
        [HttpGet]
        [Route("/user/reset-password-form")]
        public async Task<dynamic> RedirectPasswordResetAsync([FromQuery] int userId, [FromQuery] string token)
        {
            if (userId == 0 || string.IsNullOrEmpty(token))
            {
                return _serialization.SerializeMessage(400, "Invalid Request");
            }

            User user = await _userManager.FindByIdAsync(userId.ToString());
            if(user == null)
            {
                return _serialization.SerializeMessage(401, "Invalid Credentials");
            }

            if (!await _userManager.VerifyUserTokenAsync(user, _userManager.Options.Tokens.PasswordResetTokenProvider, "ResetPassword", token))
            {
                return _serialization.SerializeMessage(498, "Invalid Token");
            }
            
            return Redirect(""); //Redirects to password change form.
        }

        //Sends form data to method to reset user's password and revokes user's JWT refresh token.
        [HttpPost]
        [Route("/user/reset-password")]
        public async Task<string> ResetPasswordAsync([FromBody] UserWithPassword userWithPassword)
        {
            if (string.IsNullOrEmpty(userWithPassword.UserName) ||
                string.IsNullOrEmpty(userWithPassword.Password) ||
                string.IsNullOrEmpty(userWithPassword.Token))
            {
                return _serialization.SerializeMessage(400, "Invalid Request");
            }

            User user = await _userManager.FindByNameAsync(userWithPassword.UserName);
            JWTFactoryController jWTFactoryController = new JWTFactoryController(_userManager, _db, _serialization);
            var result = await _userManager.ResetPasswordAsync(user, userWithPassword.Token, userWithPassword.Password);

            if (result.Succeeded)
            {
                await jWTFactoryController.InvalidateRefreshTokens(user.Id);

                return "Password reset Successful";
            }
            else
                return result.Errors.ToString();
        }
    }

}