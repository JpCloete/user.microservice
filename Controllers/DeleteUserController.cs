﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Pogga.ClassTypeContracts.Models;
using ResponseSerialization;
using Users.Microservice.Interfaces;
using Users.Microservice.Models;

namespace Users.Microservice.Controllers
{
    public class DeleteUserController : Controller
    {
        private readonly UserManager<User> _userManager;
        private readonly IEmailSenderService _emailService;
        private readonly ICheckPasswordService _passwordService;
        private readonly IRedisCacheService _cacheService;
        private readonly ISerializationService _serialization;
        public DeleteUserController(UserManager<User> userManager, IEmailSenderService emailService, ICheckPasswordService passwordService, IRedisCacheService cacheService, ISerializationService serialization)
        {
            _userManager = userManager;
            _emailService = emailService;
            _passwordService = passwordService;
            _cacheService = cacheService;
            _serialization = serialization;
        }

        [HttpPost]
        [Route("/user/delete-account-request")]
        public async Task<string> DeleteUserRequestAsync([FromBody] UserWithPassword userWithPassword)
        {
            (User?, string) userTuple = await _passwordService.CheckUserPasswordAsync(userWithPassword);
            if (userTuple.Item1 == null)
            {
                if (userTuple.Item2 == "Please verify your Account by clicking on the verification link sent to your Account's Email")
                {
                    return _serialization.SerializeMessage(401, "Please verify your Account by clicking on the verification link sent to your Account's Email");
                }

                return _serialization.SerializeMessage(401, "Invalid Credentials");
            }

            var emailSendResult = await SendDeleteEmail(userWithPassword.UserName);
            if(emailSendResult == "Success")
            {
                return "Success, Please enter your Delete token sent to your Account's email.";
            }
            else
            {
                return _serialization.SerializeMessage(400, "Request Failed");
            }
        }


        [HttpPost]
        [Route("/user/delete-account-sender")]
        public async Task<string> SendDeleteEmail([FromBody] string userName)
        {
            if (string.IsNullOrEmpty(userName))
            {
                return _serialization.SerializeMessage(400, "Invalid Request");
            }

            User user = await _userManager.FindByNameAsync(userName);
            if(user == null)
            {
                return _serialization.SerializeMessage(401, "Invalid Credentials");
            }

            RNGCryptoServiceProvider provider = new RNGCryptoServiceProvider();
            var byteArray = new byte[4];

            provider.GetBytes(byteArray);

            var createToken = BitConverter.ToUInt32(byteArray, 0).ToString();
            var token = createToken.Substring(0, 6);

            await _emailService.SendEmail("test@test.com", "Delete Account", $"<p>Your Delete Token:\"{token}\"<p>", true);
            await _cacheService.SetCacheValueAsync(userName + " delete token", token);

            return "Success";
        }

        [HttpGet]
        [Route("/user/delete-account")]
        public async Task<string> DeleteAccountAsync([FromQuery] string userName, [FromQuery] string token)
        {
            if(string.IsNullOrEmpty(userName) || string.IsNullOrEmpty(token))
            {
                return _serialization.SerializeMessage(400, "Invalid Request");
            }

            string cachedToken = await _cacheService.GetCacheValueAsync(userName + " delete token");
            if(string.IsNullOrEmpty(cachedToken))
            {
                var emailSendResult = await SendDeleteEmail(userName);

                if (emailSendResult == "Success")
                {
                    return _serialization.SerializeMessage(500, "Error occured on server side, a new delete token has been sent to your Account's email.");
                }
                else
                {
                    return _serialization.SerializeMessage(500, "Error occured on server side");
                }
            }
            else if(token == cachedToken)
            {
                User user = await _userManager.FindByNameAsync(userName);
                var deleteResult = await _userManager.DeleteAsync(user);
                if(deleteResult.Succeeded)
                {
                    return "Account Successfully Deleted";
                } 
                else
                {
                    return _serialization.SerializeMessage(500, deleteResult.Errors.ToString());
                }
            }
            else
            {
                return _serialization.SerializeMessage(498, "Invalid Token");
            }
        }
    }
}
