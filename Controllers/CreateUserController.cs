﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using Users.Microservice.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using NETCore.MailKit.Core;
using Ocelot.Values;
using Pogga.ClassTypeContracts.Models;
using Newtonsoft.Json;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Authorization;
using System.Security.Principal;
using Microsoft.Extensions.Configuration;
using Users.Microservice.Controllers;
using Microsoft.AspNetCore.Http;
using Users.Microservice.Interfaces;
using ResponseSerialization;

namespace CreateUser.Microservice.Controllers
{
    public class CreateUserController : Controller
    {
        private readonly UserManager<User> _userManager;
        private readonly IEmailSenderService _emailService;
        private readonly ISerializationService _serialization;
        private readonly IDeviceIPService _devices;
        public CreateUserController(UserManager<User> userManager, IEmailSenderService emailService,
            ISerializationService serialization, IDeviceIPService devices)
        {
            _userManager = userManager;
            _emailService = emailService;
            _serialization = serialization;
            _devices = devices;
        }

        [HttpPost]
        [Route("/user/register")]
        public async Task<string> RegisterAccountAsync([FromBody] UserWithPassword userWithPassword)
        {
            if (string.IsNullOrEmpty(userWithPassword.UserName) && string.IsNullOrEmpty(userWithPassword.Email) && 
                string.IsNullOrEmpty(userWithPassword.Password) && string.IsNullOrEmpty(userWithPassword.DeviceName))
            {
                return _serialization.SerializeMessage(400, "Invalid Request");
            }

            User user = new User()
            {
                UserName = userWithPassword.UserName,
                Email = userWithPassword.Email,
            };

            var result = await _userManager.CreateAsync(user, userWithPassword.Password);

            if (result.Succeeded)
            {
                var emailSenderResult = await SendVerifyUserEmail(user.UserName);

                if (emailSenderResult == "Success")
                {
                    _devices.AddDeviceCookie(user.UserName);

                    var createdUser = await _userManager.FindByNameAsync(user.UserName);
                    var addDeviceResult = await _devices.AddDeviceAsync(createdUser.Id, userWithPassword.DeviceName);
                    if(addDeviceResult != "Success")
                    {
                        return _serialization.SerializeMessage(500, "Device already exists");
                    }

                    return "Success, Please click on the verifcation link sent to your Account's email";
                } 
                else
                {
                    return _serialization.SerializeMessage(400, "Request Failed");
                }
            }
            else
            {
                return _serialization.SerializeMessage(401, result.Errors.ToString());
            }
        }

        [HttpPost]
        [Route("/user/verify-email-sender")]
        public async Task<string> SendVerifyUserEmail([FromBody] string userName)
        {
            if(string.IsNullOrEmpty(userName))
            {
                return _serialization.SerializeMessage(400, "Invalid Request");
            }

            User user = await _userManager.FindByNameAsync(userName);
            if(user == null)
            {
                return _serialization.SerializeMessage(401, "Invalid Credentials");
            }
            
            string token = await _userManager.GenerateEmailConfirmationTokenAsync(user);
            string tokenEcoded = HttpUtility.UrlEncode(token);
            IConfiguration config = Factory.InstantiateNewConfiguration().GetConfiguration();
            string gatewayUrl = config.GetSection("Config").GetSection("GatewayApiUrl").Value;
            Uri link = new Uri("https://" + gatewayUrl + "/user/verify-email" + "?userId=" + user.Id.ToString() + "&token=" + tokenEcoded);
            await _emailService.SendEmail("test@test.com", "Verify Email", $"<a href=\"{link}\">Verfiy Account<a>", true);

            return "Success";
        }

        [HttpGet]
        [Route("/user/verify-email")]
        public async Task<string> VerifyUserEmailAsync([FromQuery] int userId, [FromQuery] string token)
        {
            if (userId == 0 && token == null)
            {
                return _serialization.SerializeMessage(400, "Invalid Request");
            }

            User user = await _userManager.FindByIdAsync(userId.ToString());
            if(user == null)
            {
                return _serialization.SerializeMessage(401, "Invaild Crendentials");
            }

            if (user.EmailConfirmed == true)
            {
                return _serialization.SerializeMessage(500, "Account has already been verified.");
            }

            var result = await _userManager.ConfirmEmailAsync(user, token);

            if (result.Succeeded)
            {
                return "Your Account has been verified.";
            }
            else
            {
                return _serialization.SerializeMessage(400, result.Errors.ToString());
            }
        }
    }
}
