﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using ResponseSerialization;
using System.Linq;
using System.Threading.Tasks;
using Users.Microservice.Models;

namespace Users.Microservice.Controllers
{
    public class GetUserController : Controller
    {
        private readonly UserDbContext _db;
        private readonly ISerializationService _serialization;

        public GetUserController(UserDbContext db, ISerializationService serialization)
        {
            _db = db;
            _serialization = serialization;
        }

        [HttpGet]
        [Route("/user/{userName}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        async public Task<string> GetUserAsync(string userName)
        {
            if (userName == null)
            {
                return _serialization.SerializeMessage(400, "Invalid Request");
            }

            var query = await _db.Users.Where(x => x.UserName.ToLower() == userName.ToLower()).Select(x => new
            {
                x.Id,
                x.UserName,
                x.PictureURL,
                x.Bio,
                x.GroupAdminId,
                followers = x.UserFollowers.Count(),
                following = x.UserFollowing.Count()
            }).FirstOrDefaultAsync();

            if (query != null)
            {
                return _serialization.SerializeObject(query);
            }
            else
                return _serialization.SerializeMessage(404, "User not found");
        }

    }
}
