﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using Pogga.ClassTypeContracts.Models;
using ResponseSerialization;
using Users.Microservice.Models;

namespace Users.Microservice.Controllers
{
    public class JWTFactoryController : Controller
    {

        private readonly IConfiguration _configuration;
        private readonly UserManager<User> _userManager;
        private readonly UserDbContext _db;
        private readonly ISerializationService _serialization;
        
        public JWTFactoryController(UserManager<User> userManager, UserDbContext db, ISerializationService serialization)
        {
            this._configuration = Factory.InstantiateNewConfiguration().GetConfiguration();
            _userManager = userManager;
            _db = db;
            _serialization = serialization;
        }

        [HttpPost]
        [Route("/user/refresh-tokens")]
        public async Task<string> RefreshTokens([FromBody] JWTRefreshAndAccessClass jWTRefreshAndAccess)
        {
            if (jWTRefreshAndAccess == null)
            {
               return _serialization.SerializeMessage(400, "Invalid Request");
            }

            AccessToken accessToken = jWTRefreshAndAccess.AccessToken;
            RefreshToken refreshToken = jWTRefreshAndAccess.RefreshToken;
            User user = await _userManager.FindByNameAsync(accessToken.Name);

            if(user == null)
            {
                return _serialization.SerializeMessage(401, "Invalid Credentials");
            }

            var dbRefreshToken = _db.RefreshTokens.Where(x => x.Id == user.Id).FirstOrDefaultAsync();
            var isValidToken = IsValidToken(accessToken.Token);

            if (isValidToken != "Valid Token" || accessToken.ExpirationDate > DateTime.UtcNow)
            {
                return _serialization.SerializeMessage(400, "Invalid Request");
            }

            if(refreshToken.ExpirationDate < DateTime.UtcNow)
            {
                return _serialization.SerializeMessage(401, "Invalid Request, Token expired. Please login again.");
            }

            var token = await CreateRefreshToken(user);

            var newRefreshToken = new RefreshToken()
            {
                Id = refreshToken.Id,
                Token = token.Token,
                CreationDate = DateTime.UtcNow,
                ExpirationDate = DateTime.UtcNow.AddYears(1),
                UserId = user.Id
            };

            _db.RefreshTokens.Attach(newRefreshToken);
            _db.Entry(newRefreshToken).Property(x => x.CreationDate).IsModified = true;
            _db.Entry(newRefreshToken).Property(x => x.ExpirationDate).IsModified = true;
            _db.Entry(newRefreshToken).Property(x => x.Token).IsModified = true;
            await _db.SaveChangesAsync();

            JWTRefreshAndAccessClass tokens = new JWTRefreshAndAccessClass
            {
                AccessToken = CreateAccessToken(user),
                RefreshToken = newRefreshToken
            };

            return _serialization.SerializeObject(tokens);
        }

        public AccessToken CreateAccessToken(User user)
        {

            ICollection<Claim> claims = new List<Claim>()
            {
                new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
                new Claim(ClaimTypes.Name, user.UserName),
                new Claim(ClaimTypes.Email, user.Email),
            };

            var token = CreateTokenKey(claims);

            AccessToken jwtObject = new AccessToken
            {
                Token = new JwtSecurityTokenHandler().WriteToken(token),
                CreationDate = DateTime.UtcNow,
                ExpirationDate = DateTime.UtcNow.AddMinutes(15),
                Id = user.Id,
                Name = user.UserName,
                Email = user.Email
            };

            return jwtObject;
        }

        public async Task<RefreshToken> CreateRefreshToken(User user)
        {
            var randomNumber = new byte[32];
            var rng = RandomNumberGenerator.Create();

            rng.GetBytes(randomNumber);

            var token = Convert.ToBase64String(randomNumber);

            RefreshToken refreshToken = new RefreshToken()
            {
                Token = token,
                ExpirationDate = DateTime.UtcNow.AddYears(1),
                UserId = user.Id,
            };

            await _db.RefreshTokens.AddAsync(refreshToken);
            await _db.SaveChangesAsync();

            return refreshToken;
        }

        public async Task InvalidateRefreshTokens(int userId)
        {
            var refreshTokens = _db.RefreshTokens.Where(x => x.UserId == userId);

            foreach(var refreshToken in refreshTokens)
            {
                _db.RefreshTokens.Remove(refreshToken);
            }
  
            await _db.SaveChangesAsync(); 
        }

        public JwtSecurityToken CreateTokenKey(ICollection<Claim> claims)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Jwt:Key"])); //salt
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            return new JwtSecurityToken(_configuration["Jwt:Issuer"],
                _configuration["Jwt:Issuer"],
                claims,
                expires: DateTime.UtcNow.AddMinutes(15),
                signingCredentials: credentials);
        }

        public string IsValidToken(string token)
        {
            var tokenValidationParameters = new TokenValidationParameters
            {
                ValidateIssuer = true,
                ValidateAudience = true,
                ValidateLifetime = true,
                ValidateIssuerSigningKey = true,
                ValidIssuer = _configuration["Jwt:Issuer"],
                ValidAudience = _configuration["Jwt:Issuer"],
                IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Jwt:Key"]))
            };

            var tokenHandler = new JwtSecurityTokenHandler();
            var principal = tokenHandler.ValidateToken(token, tokenValidationParameters, out SecurityToken securityToken);
            var jwtSecurityToken = securityToken as JwtSecurityToken;
            if (jwtSecurityToken == null ||
                !jwtSecurityToken.Header.Alg.Equals(SecurityAlgorithms.HmacSha256, StringComparison.InvariantCultureIgnoreCase) ||
                principal == null)
            {
                return "Invalid token";
            }

            return "Valid Token";
        }
        
    }
}
