﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Pogga.ClassTypeContracts.Models;
using ResponseSerialization;
using Users.Microservice.Models;

namespace Users.Microservice.Controllers
{
    public class SignOutUserController : Controller
    {
        private readonly UserDbContext _db;
        private readonly ISerializationService _serialization;
        public SignOutUserController(UserDbContext db, ISerializationService serialization)
        {
            _db = db;
            _serialization = serialization;
        }

        [HttpPost]
        [Route("/user/sign-out")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<string> SignOutUserAsync([FromBody] int tokenId)
        {
            if (tokenId == 0)
            {
                return _serialization.SerializeMessage(400, "Invalid Request");
            }

            var refreshToken = await _db.RefreshTokens.Where(x => x.Id == tokenId).FirstOrDefaultAsync();

            _db.RefreshTokens.Remove(refreshToken);

            return "Sign out attempt Successful.";
        }

    }
}
