﻿using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Pogga.ClassTypeContracts.Models;
using Users.Microservice.Models;
using Users.Microservice.Controllers;
using System.Security.Cryptography;
using System.Web;
using Microsoft.Extensions.Caching.Distributed;
using Users.Microservice.Interfaces;
using ResponseSerialization;
using System.Net;
using Newtonsoft.Json.Linq;
using Microsoft.EntityFrameworkCore;

namespace Users.Microservice.Controllers
{
    public class SignInUserController : Controller
    {
        private readonly UserManager<User> _userManager;
        private readonly UserDbContext _db;
        private readonly IEmailSenderService _emailService;
        private readonly ISerializationService _serialization;
        private readonly ICheckPasswordService _passwordService;
        private readonly IRedisCacheService _cache;
        private readonly IDeviceIPService _devices;
        public SignInUserController(UserManager<User> userManager, IEmailSenderService emailService,
            UserDbContext db, ICheckPasswordService passwordService, IRedisCacheService cache,
            ISerializationService serialization, IDeviceIPService devices)
        {
            _userManager = userManager;
            _db = db;
            _emailService = emailService;
            _serialization = serialization;
            _passwordService = passwordService;
            _cache = cache;
            _devices = devices;
        }

        [HttpPost]
        [Route("/user/sign-in")]
        public async Task<string> SignInUserAsync([FromBody] UserWithPassword userWithPassword)
        {
            bool ip4Result = _devices.ValidateIP(userWithPassword.Ip);
            if (ip4Result == false || string.IsNullOrEmpty(userWithPassword.DeviceName))
            {
                return "Invalid Ip or device";
            }

            (User, string) userTuple = await _passwordService.CheckUserPasswordAsync(userWithPassword);
            string responseMessage = userTuple.Item2;
            if (responseMessage == "Invalid Request" || responseMessage == "Invalid Credentials")
            {
                return _serialization.SerializeMessage(401, "Invalid Credentials");
            }
            else if (responseMessage == "Please verify your Email by clicking on the verification link sent to your Account's Email")
            {
                return _serialization.SerializeMessage(401, "Please verify your Account by clicking on the verification link sent to your Account's Email");
            }
            else if (responseMessage == "Invalid Password")
            {
                string addSignInResult = await _devices.AddSignInAsync(userTuple.Item1.Id, userWithPassword.Ip, false);

                return _serialization.SerializeMessage(401, "Invalid Credentials");
            }

            User user = userTuple.Item1;
            if (_devices.VerifyCookie("DeviceCookie" + user.UserName) == false)//checks if DeviceCookie exists
            {
                if (_devices.VerifyCookie("VerifyDeviceCookie" + user.UserName))//VerifyDeviceCookie used to prevent token conflicts for a single device
                {
                    return "Please verify your device";
                }

                var deviceResult = await _db.Devices
                    .Where(x => x.UserId == user.Id)
                    .Where(x => x.DeviceName == userWithPassword.DeviceName)
                    .FirstOrDefaultAsync();
                if (deviceResult != null)//If cookie doesn't exist, check if device is in db
                {
                    _devices.AddDeviceCookie(user.UserName);
                }
                else
                {
                    var emailSendResult = await SendVerifyDeviceEmail(user.UserName, userWithPassword.DeviceName);
                    if (emailSendResult == "Success")
                    {
                        _devices.AddVerifyDeviceCookie(user.UserName, userWithPassword.DeviceName);

                        return "Success, Please enter your verification token sent to your Account's email.";
                    }
                    else
                    {
                        return _serialization.SerializeMessage(400, "Request Failed");
                    }
                }

            }
            await _devices.AddSignInAsync(userTuple.Item1.Id, userWithPassword.Ip, false);

            return await GetJWTTokensAsync(user);
        }

        [HttpPost]
        [Route("verify-device-sender")]
        public async Task<string> SendVerifyDeviceEmail([FromBody] string userName, string deviceName)
        {
            if (string.IsNullOrEmpty(userName))
            {
                return _serialization.SerializeMessage(400, "Invalid Request");
            }

            User user = await _userManager.FindByNameAsync(userName);
            if (user == null)
            {
                return _serialization.SerializeMessage(401, "Invalid Credentials");
            }

            RNGCryptoServiceProvider provider = new RNGCryptoServiceProvider();
            var byteArray = new byte[4];

            provider.GetBytes(byteArray);

            var createToken = BitConverter.ToUInt32(byteArray, 0).ToString();
            string token = createToken.Substring(0, 6).Trim('"'); //Creates random 6 length number used for verifying device

            await _cache.SetCacheValueAsync(user.UserName + " " + deviceName + " device token", token);
            await _emailService.SendEmail("test@test.com", "New Device login", $"<p>Your Verification Token:\"{token}\"<p>", true);

            return "Success";
        }


        [HttpPost]
        [Route("/user/verify-device")]
        public async Task<string> VerifyDeviceAsync([FromBody] UserWithPassword userWithPassword)
        {
            if (_devices.VerifyCookie("DeviceCookie" + userWithPassword.UserName) &&
               _devices.CompareCookie("DeviceCookie" + userWithPassword.UserName, userWithPassword.UserName))
            {
                return _serialization.SerializeMessage(500, "Device already verified");
            }

            if (string.IsNullOrEmpty(userWithPassword.UserName) || string.IsNullOrEmpty(userWithPassword.Token) ||
                string.IsNullOrEmpty(userWithPassword.DeviceName) || _devices.CompareCookie("VerifyDeviceCookie" + userWithPassword.UserName, userWithPassword.DeviceName) == false)
            {
                return _serialization.SerializeMessage(401, "Invalid Request");
            }

            string cachedToken = await _cache.GetCacheValueAsync(userWithPassword.UserName + " " + userWithPassword.DeviceName + " device token");
            if (string.IsNullOrEmpty(cachedToken))
            {
                _devices.DeleteCookie(_devices.EncryptCookie("VerifyDeviceCookie" + userWithPassword.UserName, 32));

                return _serialization.SerializeMessage(500, "Internal Server Error, Please Login again to receive a new device verification token");
            }
            else if (userWithPassword.Token == cachedToken)
            {
                _devices.DeleteCookie(_devices.EncryptCookie("VerifyDeviceCookie" + userWithPassword.UserName, 32));
                _devices.AddDeviceCookie(userWithPassword.UserName);

                await _cache.DeleteCacheValueAsync(userWithPassword.UserName + " " + userWithPassword.DeviceName + " device token");
                User user = await _userManager.FindByNameAsync(userWithPassword.UserName);
                var addDeviceResult = await _devices.AddDeviceAsync(user.Id, userWithPassword.DeviceName);
                if (addDeviceResult != "Success")
                {
                    return _serialization.SerializeMessage(500, "Device already exists");
                }

                return await GetJWTTokensAsync(user);
            }
            else
            {
                return _serialization.SerializeMessage(498, "Invalid Token");
            }
        }

        public async Task<string> GetJWTTokensAsync(User user)
        {
            var jwtFactory = new JWTFactoryController(_userManager, _db, _serialization);
            RefreshToken refreshToken = await jwtFactory.CreateRefreshToken(user);
            AccessToken accessToken = jwtFactory.CreateAccessToken(user);
            JWTRefreshAndAccessClass jWTRefreshAndAccessClass = new JWTRefreshAndAccessClass()
            {
                AccessToken = accessToken,
                RefreshToken = refreshToken
            };

            return _serialization.SerializeObject(jWTRefreshAndAccessClass);
        }
    }
}
