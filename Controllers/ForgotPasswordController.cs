﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Pogga.ClassTypeContracts.Models;
using ResponseSerialization;
using Users.Microservice.Interfaces;
using Users.Microservice.Models;

namespace Users.Microservice.Controllers
{
    public class ForgotPasswordController : Controller
    {
        private readonly UserManager<User> _userManager;
        private readonly IEmailSenderService _emailService;
        private readonly ISerializationService _serialization;
            
        public ForgotPasswordController(UserManager<User> userManager, IEmailSenderService emailService, ISerializationService serialization)
        {
            _userManager = userManager;                   
            _emailService = emailService;               
            _serialization = serialization;
        }

        //Used in combination with reset-password-form -and reset-password endpoints in ResetPasswordController 
        [HttpPost]
        [Route("/user/forgot-password-request")]
        public async Task<string> ForgotPasswordRequestAsync([FromBody] UserWithPassword userWithPassword)
        {
            User user = new User();
            if(string.IsNullOrEmpty(userWithPassword.UserName) && string.IsNullOrEmpty(userWithPassword.Email))
            {
                return _serialization.SerializeMessage(400, "Invalid Request");
            }

            if(!string.IsNullOrEmpty(userWithPassword.UserName))
            {
                user = await _userManager.FindByNameAsync(userWithPassword.UserName);
            }
            
            if(!string.IsNullOrEmpty(userWithPassword.Email))
            {
                user = await _userManager.FindByEmailAsync(userWithPassword.Email);
            }

            if(user == null)
            {
                return _serialization.SerializeMessage(401, "Invalid Credentials");
            }

            var token = await _userManager.GeneratePasswordResetTokenAsync(user);
            string tokenEcoded = HttpUtility.UrlEncode(token);
            IConfiguration config = Factory.InstantiateNewConfiguration().GetConfiguration();
            string gatewayUrl = config.GetSection("Config").GetSection("GatewayApiUrl").Value;
            Uri link = new Uri("https://" + gatewayUrl + "/user/reset-password-form" + "?userId=" + user.Id.ToString() + "&token=" + tokenEcoded);

            await _emailService.SendEmail("test@test.com", "Forgot Password", $"<a href=\"{link}\">Click here to reset your account's password.<a>", true);

            return "Click the Forgot Password link sent to your Account's Email.";
        }
    }
}
