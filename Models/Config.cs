﻿using CreateUser.Microservice;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration.Json;
using Microsoft.Extensions.Configuration.EnvironmentVariables;
using Microsoft.Extensions.Configuration.FileExtensions;

namespace Users.Microservice.Models
{
    public class Config
    {
        private readonly IConfiguration configuration;
        public Config()
        {
            //ConfigureBuilder used to avoid dependency injection with IConfiguration.
            var builder = new ConfigurationBuilder()
                        .SetBasePath(Directory.GetCurrentDirectory())
                        .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                        .AddEnvironmentVariables();

            this.configuration = builder.Build();
        }

        public IConfiguration GetConfiguration()
        {
            return configuration;
        }

    }
}
