﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static Users.Microservice.Controllers.JWTFactoryController;

namespace Users.Microservice.Models
{
    public class JWTRefreshAndAccessClass
    {
        public AccessToken AccessToken { get; set; }

        public RefreshToken RefreshToken { get; set; }
    }
}
