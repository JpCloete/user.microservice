﻿using Pogga.ClassTypeContracts.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Users.Microservice.Models
{
    public class UserWithPassword : User
    {
        new public virtual string UserName { get; set; }
        new public virtual string Email { get; set; }
        public virtual string Password { get; set; }
        public virtual string Token { get; set; }
        public virtual string DeviceName { get; set; }
        public virtual string Ip { get; set; }
    }
}
