﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Pogga.ClassTypeContracts.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Users.Microservice.Models
{
    public class UserDbContext : IdentityDbContext<User, IdentityRole<int>, int>
    {
        public UserDbContext(DbContextOptions<UserDbContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Ignore<EventContent>();
            modelBuilder.Ignore<Content>();
            modelBuilder.Ignore<Comment>();
            modelBuilder.Ignore<Challenge>();
            modelBuilder.Ignore<Group>();
            modelBuilder.Ignore<Event>();
            modelBuilder.Ignore<Post>();

            modelBuilder.Entity<User>(x =>
            {   
                x.HasMany(y => y.UserFollowers)
                    .WithOne()
                    .IsRequired(false);

                x.HasMany(y => y.UserFollowing)
                    .WithOne()
                    .IsRequired(false);

                x.HasMany(y => y.ActiveChallenges)
                    .WithOne()  
                    .IsRequired(false);

                x.HasMany(y => y.CompletedChallenges)
                    .WithOne()
                    .IsRequired(false);

                x.HasMany(y => y.GroupMembers)
                    .WithOne()
                    .IsRequired(false);

                x.HasMany(y => y.GroupFollowing)
                    .WithOne()
                    .IsRequired(false);

                x.HasMany(y => y.EventFollowing)
                    .WithOne()
                    .IsRequired(false);

                x.HasMany(y => y.EventAdmin)
                    .WithOne(y => y.Admin)
                    .HasForeignKey(y => y.AdminId)
                    .IsRequired(false);

                x.HasOne(y => y.GroupAdmin)
                    .WithOne(y => y.Admin)
                    .HasForeignKey<Group>(y => y.AdminId)
                    .IsRequired(false);

                x.HasMany(y => y.Posts)
                    .WithOne(y => y.Poster)
                    .HasForeignKey(y => y.PosterId)
                    .IsRequired(false);

                x.HasMany(y => y.Comments)
                    .WithOne(y => y.Commenter)
                    .HasForeignKey(y => y.CommenterId)
                    .IsRequired(false);
            });

            modelBuilder.Entity<RefreshToken>(x =>
            {
                x.HasKey(y => y.Id);
            });

            modelBuilder.Entity<Device>(x =>
            {
                x.HasKey(y => y.Id);
            });

            modelBuilder.Entity<SignIn>(x =>
            {
                x.HasKey(y => y.Id);
            });
        }
        public override DbSet<User> Users { get; set; }
        public DbSet<RefreshToken> RefreshTokens { get; set; }
        public DbSet<Device> Devices { get; set; }
        public DbSet<SignIn> SignIns { get; set; }
    }
}
