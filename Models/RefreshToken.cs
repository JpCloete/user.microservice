﻿using Microsoft.AspNetCore.Identity;
using Pogga.ClassTypeContracts.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Users.Microservice.Models
{
    public class RefreshToken
    {
        public RefreshToken()
        {
            CreationDate = DateTime.UtcNow;
        }
        public int Id { get; set; }
        public string Token { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime ExpirationDate { get; set; }
        public int UserId { get; set; }
    }
}
