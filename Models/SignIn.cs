﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Users.Microservice.Models
{
    public class SignIn
    {
        public SignIn()
        {
            SignInDate = DateTime.UtcNow;
        }
        public int Id { get; set; }
        public string Ip { get; set; }
        public string City { get; set; }
        public string Region { get; set; }
        public string Country { get; set; }
        public string Continent { get; set; }
        public DateTime SignInDate { get; set; }
        public int Count { get; set; }
        public bool Successful { get; set; }
        public int UserId { get; set; }
        
    }
}
