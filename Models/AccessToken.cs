﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Users.Microservice.Models
{
    public class AccessToken
    {
        public string Token { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime ExpirationDate { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
    }
}
