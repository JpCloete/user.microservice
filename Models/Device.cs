﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Users.Microservice.Models
{
    public class Device
    {
        public Device()
        {
            AddedDate = DateTime.UtcNow;
        }
        public int Id { get; set; }
        public string DeviceName { get; set; }
        public DateTime AddedDate { get; set; }
        public int UserId { get; set; }
    }
}