﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Users.Microservice.Migrations
{
    public partial class Updated_SignIn_Table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Continent",
                table: "SignIns",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Continent",
                table: "SignIns");
        }
    }
}
