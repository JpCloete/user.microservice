﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Pogga.ClassTypeContracts.Models;
using Users.Microservice.Interfaces;
using Users.Microservice.Models;

namespace Users.Microservice.Controllers
{
    public class CheckPasswordService : ICheckPasswordService
    {
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        public CheckPasswordService(UserManager<User> userManager, SignInManager<User> signInManager)
        {
            _signInManager = signInManager;
            _userManager = userManager;
        }

        public async Task<(User?, string)> CheckUserPasswordAsync(UserWithPassword userWithPassword)
        {
            if (string.IsNullOrEmpty(userWithPassword.Password))
                return (null, "Invalid Request");

            User user = new User();

            if (!string.IsNullOrEmpty(userWithPassword.Email)) //User for Email sign-in
            {
                user = await _userManager.FindByEmailAsync(userWithPassword.Email);
            } 

            if (!string.IsNullOrEmpty(userWithPassword.UserName)) //Used for 
            {
                user = await _userManager.FindByNameAsync(userWithPassword.UserName);
            }

            if (user == null)
                return (null, "Invalid Credentials");

            var isEmailVerified = await _userManager.IsEmailConfirmedAsync(user);

            if (!isEmailVerified)
            {
                return (null, "Please verify your Account by clicking on the verification link sent to your Account's Email");
            }

            var result = await _signInManager.CheckPasswordSignInAsync(user, userWithPassword.Password, false);

            if (result.Succeeded)
                return (user, "Success");
            else
                return (user, "Invalid Password");
        }
    }
}
