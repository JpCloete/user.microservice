﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;
using ResponseSerialization;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Threading.Tasks;
using Users.Microservice.Interfaces;
using Users.Microservice.Models;

namespace Users.Microservice.Services
{
    public class DeviceIPService : IDeviceIPService
    {
        private readonly UserDbContext _db;
        private readonly HttpResponse _response;
        private readonly HttpRequest _request;
        private readonly AesCryptoServiceProvider _crypto;
        public DeviceIPService(UserDbContext db, IHttpContextAccessor httpContextAccessor, AesCryptoServiceProvider crypto)
        {
            _db = db;
            _response = httpContextAccessor.HttpContext.Response;
            _request = httpContextAccessor.HttpContext.Request;
            _crypto = crypto;
        }

        public bool ValidateIP(string ipString)
        {
            if (String.IsNullOrWhiteSpace(ipString))
            {
                return false;
            }

            string[] splitValues = ipString.Split('.');
            if (splitValues.Length != 4)
            {
                return false;
            }

            return splitValues.All(r => byte.TryParse(r, out byte tempForParsing));
        }

        public async Task<string> AddSignInAsync(int userId, string ip, bool successful)
        {
            var signInExists = await _db.SignIns
                    .Where(x => x.UserId == userId)
                    .Where(x => x.Ip == ip)
                    .FirstOrDefaultAsync();

            if (signInExists != null)
            {
                signInExists.Count++;
                signInExists.SignInDate = DateTime.UtcNow;
                signInExists.Successful = successful;
                await _db.SaveChangesAsync();

                return "Success";
            }
            else
            {
                WebClient webClient = new WebClient();
                var ipInfoString = webClient.DownloadString("https://api.ipdata.co/" + ip +
                    "?api-key=c08b79d757bdd9471b2b1ec0f2850749a53d717712f1048f0c904fec&fields=ip,city,region,country_name,continent_name");

                var ipInfo = JObject.Parse(ipInfoString);
                if (ipInfo["continent_name"] == null)
                {
                    return "Invalid Ip";
                }

                SignIn signIn = new SignIn()
                {
                    Ip = ip,
                    City = ipInfo["city"].ToString(),
                    Region = ipInfo["region"].ToString(),
                    Country = ipInfo["country_name"].ToString(),
                    Continent = ipInfo["continent_name"].ToString(),
                    Count = 1,
                    Successful = successful,
                    UserId = userId
                };

                await _db.SignIns.AddAsync(signIn);
                await _db.SaveChangesAsync();

                return "Success";
            }
        }

        public async Task<string> AddDeviceAsync(int userId, string deviceName)
        {
            var signInExists = await _db.Devices
                    .Where(x => x.UserId == userId)
                    .Where(x => x.DeviceName == deviceName)
                    .FirstOrDefaultAsync();

            if (signInExists != null)
            {
                return "Device already exists";
            }
            else
            {
                Device device = new Device()
                {
                    DeviceName = deviceName,
                    UserId = userId
                };

                await _db.Devices.AddAsync(device);
                await _db.SaveChangesAsync();

                return "Success";
            }
        }

        public void AddDeviceCookie(string userName)
        {
            var options = new CookieOptions
            {
                Expires = DateTime.UtcNow.AddYears(1),
                HttpOnly = true,
                Secure = true
            };

            _response.Cookies.Append(EncryptCookie("DeviceCookie" + userName, 32), EncryptCookie(userName, 24), options);
        }

        public void AddVerifyDeviceCookie(string userName, string deviceName)
        {
            var options = new CookieOptions
            {
                Expires = DateTime.UtcNow.AddYears(1),
                HttpOnly = true,
                Secure = true
            };

            _response.Cookies.Append(EncryptCookie("VerifyDeviceCookie" + userName, 32), EncryptCookie(deviceName, 24), options);
        }

        public void DeleteCookie(string cookieKey)
        {
            _response.Cookies.Delete(cookieKey);
        }

        public string EncryptCookie(string cookie, int key)
        {
            _crypto.Key = new byte[key];
            _crypto.IV = System.Text.Encoding.UTF8.GetBytes("Erikenruan1704is");
            string result;

            ICryptoTransform encryptor = _crypto.CreateEncryptor(_crypto.Key, _crypto.IV);

            using (MemoryStream msEncrypt = new MemoryStream())
            {
                using CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write);
                using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                {
                    swEncrypt.Write(cookie);
                }
                byte[] encrypted = msEncrypt.ToArray();
                result = Convert.ToBase64String(encrypted);
            }

            return result;
        }

        public bool VerifyCookie(string cookie)
        {
            if (_request.Cookies.Keys.Contains(EncryptCookie(cookie, 32)))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool CompareCookie(string cookie, string cookieValue)
        {
            if (_request.Cookies[EncryptCookie(cookie, 32)] == EncryptCookie(cookieValue, 24))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

    }
}
