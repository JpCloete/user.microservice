﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using NETCore.MailKit;
using NETCore.MailKit.Core;
using NETCore.MailKit.Infrastructure.Internal;
using Newtonsoft.Json;
using Microsoft.Extensions.Configuration.Binder;
using Users.Microservice.Models;
using Users.Microservice.Interfaces;

namespace Users.Microservice.Controllers
{
    public class EmailSenderService : IEmailSenderService
    {
        private readonly IEmailService _emailService;

        public EmailSenderService(IEmailService emailService)
        {
            _emailService = emailService;
        }

        public async Task SendEmail(string mailTo, string subject, string message, bool isHtml)
        {
            await _emailService.SendAsync(mailTo, subject, message, isHtml);
        }
    }
}
