﻿using Pogga.ClassTypeContracts.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Users.Microservice.Models;

namespace Users.Microservice.Interfaces
{
    public interface ICheckPasswordService
    {

        Task<(User?, string)> CheckUserPasswordAsync(UserWithPassword userWithPassword);
    }
}
