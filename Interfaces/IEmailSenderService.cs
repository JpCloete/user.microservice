﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Users.Microservice.Interfaces
{
    public interface IEmailSenderService
    {
        Task SendEmail(string mailTo, string subject, string message, bool isHtml);
    }
}
