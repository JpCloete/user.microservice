﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Users.Microservice.Interfaces
{
    public interface IDeviceIPService
    {
        bool ValidateIP(string ipString);

        Task<string> AddSignInAsync(int userId, string ip, bool successful);

        Task<string> AddDeviceAsync(int userId, string deviceName);

        void AddDeviceCookie(string userName);

        void AddVerifyDeviceCookie(string userName, string deviceName);

        void DeleteCookie(string cookieKey);

        string EncryptCookie(string userName, int key);

        bool VerifyCookie(string cookie);

        bool CompareCookie(string cookie, string cookieValue);

    }
}
