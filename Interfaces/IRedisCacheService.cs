﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Users.Microservice.Interfaces
{
    public interface IRedisCacheService
    {
        Task<string> GetCacheValueAsync(string key);

        Task SetCacheValueAsync(string key, string value);

        Task DeleteCacheValueAsync(string key);
    }
}
